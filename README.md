> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=299715)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# cMd
> v0.1a

Hello guys!  
Today, I release some very small command processor, like almost everyone
else did 

It's very fast and REALLY SMALL (some test returned a time of 3ms!
Please test it yourself though)

## Usage
> (also included to the include 😛)

-	Use `do_cMd;` in `OnPlayerCommandText` to call the command like that:
	```c
	public OnPlayerCommandText(playerid, cmdtext[])
	{
		// code ...
		do_cMd;
		// code ...
	}
	```

-	Define Commands like that (**<span style="color: red">PLEASE USE LOWERCASE LETTERS</span>**):
	```c
	cMd:command_name;
	{
		// code ...
	}
	```
... as a standalone function like public or stock.

\- playerid and `params[]` are available in every command, `params[]`
includes the command's parameters you can split with `sscanf`.

(Optional: you can use the variable `cMd` after `do_cMd;` to check if a
command was called. It should return `1` then.)

## Changelog
**[v0.1a]**:
- Made commands non-casesensitive. Please define commands with lowercase names

**[v0.1]**:
- First release


PS: Critics and comments are welcome 
